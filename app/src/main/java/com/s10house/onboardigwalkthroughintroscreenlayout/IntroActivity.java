package com.s10house.onboardigwalkthroughintroscreenlayout;

import androidx.appcompat.app.AppCompatActivity;


import android.content.Intent;
import android.content.SharedPreferences;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class IntroActivity extends AppCompatActivity {

    private ViewPager screenPager;
    IntroViewPagerAdapter introViewPagerAdapter;
    TabLayout tabLayoutIndicator;
    Button btnIntroNext;
    TextView textViewSkip;
    int position = 0;
    Button btnGetStarted;
    Animation btnGetStartedAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        // make activity on full screen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //when this activity is about to be launched we need to check if it is opened before or not

        if (restorePrefData()){

            Intent mainActivity = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(mainActivity);
            finish();

        }

        setContentView(R.layout.activity_intro);

        // hiding action bar

        getSupportActionBar().hide();

        //ini views

        textViewSkip = findViewById(R.id.textview_skip);

        btnIntroNext = findViewById(R.id.btn_intro_next);

        btnGetStarted = findViewById(R.id.btn_get_started);

        tabLayoutIndicator = findViewById(R.id.tab_layout_indicator);

        btnGetStartedAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.btn_getstarted_animation);

        //fill list screen
        final List<ScreenItemView> mList = new ArrayList<>();
        mList.add(new ScreenItemView("Fresh Food","Lorem ipsum dolor sit amet, consecteturadipiscing elit, sed do eiusmod tempor incididunt et labore et dolore magna aliqua, consectetur consectetur adipiscing elit.",R.drawable.img1));
        mList.add(new ScreenItemView("Fast Delivery","Lorem ipsum dolor sit amet, consecteturadipiscing elit, sed do eiusmod tempor incididunt et labore et dolore magna aliqua, consectetur consectetur adipiscing elit.",R.drawable.img2));
        mList.add(new ScreenItemView("Easy Payment","Lorem ipsum dolor sit amet, consecteturadipiscing elit, sed do eiusmod tempor incididunt et labore et dolore magna aliqua, consectetur consectetur adipiscing elit.",R.drawable.img3));

        // setup viewpager
        screenPager = findViewById(R.id.screen_viewpager);
        introViewPagerAdapter = new IntroViewPagerAdapter(this, mList);
        screenPager.setAdapter(introViewPagerAdapter);

        // setup tabLayoutIndicator with viewPager
        tabLayoutIndicator.setupWithViewPager(screenPager);

        // next button click Listener
        btnIntroNext.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                position = screenPager.getCurrentItem();
                if (position< mList.size()){

                    position++;
                    screenPager.setCurrentItem(position);
                }

                // when we reach to last screen
                if (position == mList.size() - 1){

                    // TODO : show the GETSTARTED Button and hide the indicators and the NEXT Button

                    loadLastScreen();
                }
            }
        });

        // tabLayoutIndicator add changeListener
        tabLayoutIndicator.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == mList.size() - 1){

                 loadLastScreen();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        // Get Started Button clickListener
        btnGetStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //open Main Activity
                Intent mainActivity = new Intent(getApplicationContext(),MainActivity.class);
    //common-mistake to not use of startActivity
                startActivity(mainActivity);

                // also we need to save boolean value to storage so next time when the user run the app
                // we could know that he is already checked the introScreenActivity
                // i'm going to use shared preferences to that process

                savePrefsData();
                finish();
            }
        });

        //skip buttonClickListener

        textViewSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                screenPager.setCurrentItem(mList.size());
            }
        });

    }

    private boolean restorePrefData() {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("myPrefs", MODE_PRIVATE);
        Boolean isIntroActivityOpenedBefore = pref.getBoolean("isIntroOpened", false);
        return isIntroActivityOpenedBefore;

    }

    private void savePrefsData() {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("myPrefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("isIntroOpened", true);
        editor.commit();

    }

    // show the GETSTARTED Button and hide the Indicators and the NEXT Button
    private void loadLastScreen() {
        textViewSkip.setVisibility(View.INVISIBLE);
        btnIntroNext.setVisibility(View.INVISIBLE);
        tabLayoutIndicator.setVisibility(View.INVISIBLE);
        btnGetStarted.setVisibility(View.VISIBLE);
        // TODO : ADD an Animation for GETSTARTED Button

        //setup animation getStarted
        btnGetStarted.setAnimation(btnGetStartedAnimation);

    }
}
